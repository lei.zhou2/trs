..
 # Copyright (c) 2023, Linaro Ltd
 #
 # SPDX-License-Identifier: MIT

################
Developer Manual
################

.. toctree::
   :titlesonly:
   :maxdepth: 2
   :caption: Contents

   architectures
   user_accounts
   build_system
   yocto_layers
   security
   feature
