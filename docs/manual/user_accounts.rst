..
 # Copyright (c) 2022, Arm Limited.
 # Copyright (c) 2023, Linaro Ltd.
 #
 # SPDX-License-Identifier: MIT

#############
User Accounts
#############
TRS leverage the EWAOL distribution user configuration, which has defined the
following user accounts:

 * ``root`` with administrative privileges enabled by default. The login is
   disabled if ``ewaol-security`` is included in ``DISTRO_FEATURES``.
 * ``ewaol`` with administrative privileges enabled with ``sudo``.
 * ``user``  without administrative privileges.
 * ``test`` with administrative privileges enabled with ``sudo``. This account
   is created only if ``ewaol-test`` is included in ``DISTRO_FEATURES``.

By default, each users account has disabled password. The default
administrative group name is ``sudo``. Other sudoers configuration is included
in ``meta-ewaol-distro/recipes-extended/sudo/files/ewaol_admin_group.in``.
For virtualization images, above user accounts are created for Control VM and
Guest VM domains.

..
  TODO: We probably don't want to link to the external page for this, since we
 can be a it confusing for the user who might not notice that he ended up at
 another documentation page.

If ``ewaol-security`` is included in ``DISTRO_FEATURES``, each user is prompted
to a set new password on first login. For more information about security see
`Security Hardening`_.

..
  TODO: Check with Mikko, whether this is true or not for TRS.

All ``validation_run-time_integration_tests`` are executed as the ``test``
user.

.. _Security Hardening: https://ewaol.docs.arm.com/en/kirkstone-dev/manual/hardening.html
