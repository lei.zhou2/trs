#
# SPDX-License-Identifier: MIT
#

from subprocess import Popen, PIPE
import time

from oeqa.runtime.case import OERuntimeTestCase
from oeqa.core.decorator.depends import OETestDepends
from oeqa.core.decorator.oetimeout import OETimeout
from oeqa.core.decorator.data import skipIfQemu

class TRSRebootTest(OERuntimeTestCase):

    @OETimeout(200)
    @OETestDepends(['ssh.SSHTest.test_ssh'])
    def test_reboot(self):
        cmd = 'cat /etc/machine-id'
        status, output = self.target.run(cmd)
        self.assertEqual(status, 0, msg='\n'.join([cmd, output]))
        machine_id1 = output

        cmd = 'cat /proc/sys/kernel/random/boot_id'
        status, output = self.target.run(cmd)
        self.assertEqual(status, 0, msg='\n'.join([cmd, output]))
        boot_id1 = output

        cmd = 'uptime -s'
        status, output = self.target.run(cmd)
        self.assertEqual(status, 0, msg='\n'.join([cmd, output]))
        uptime1 = output

        cmd = 'mount | grep rootfs'
        status, output = self.target.run(cmd)
        self.assertEqual(status, 0, msg='\n'.join([cmd, output]))
        self.assertEqual(output, '/dev/mapper/rootfs on / type ext4 (rw,noatime)', msg='\n'.join([cmd, output]))

        cmd = 'reboot'
        status, output = self.target.run(cmd)
        self.assertEqual(status, 0, msg='\n'.join([cmd, output]))

        # enough time for a reboot into already encrypted rootfs so
        # this should be fast
        time.sleep(120)

        cmd = 'cat /etc/machine-id'
        status, output = self.target.run(cmd)
        self.assertEqual(status, 0, msg='\n'.join([cmd, output]))
        machine_id2 = output

        cmd = 'cat /proc/sys/kernel/random/boot_id'
        status, output = self.target.run(cmd)
        self.assertEqual(status, 0, msg='\n'.join([cmd, output]))
        boot_id2 = output

        cmd = 'uptime -s'
        status, output = self.target.run(cmd)
        self.assertEqual(status, 0, msg='\n'.join([cmd, output]))
        uptime2 = output

        cmd = 'mount | grep rootfs'
        status, output = self.target.run(cmd)
        self.assertEqual(status, 0, msg='\n'.join([cmd, output]))
        self.assertEqual(output, '/dev/mapper/rootfs on / type ext4 (rw,noatime)', msg='\n'.join([cmd, output]))

        msg = "\nmachine-id %s should not change but was %s" % (machine_id1, machine_id2)
        self.assertEqual(machine_id1, machine_id2, msg)

        msg = "\nboot_id %s should change but was %s" % (boot_id1, boot_id2)
        self.assertNotEqual(boot_id1, boot_id2, msg)

        msg = "\nUptime since %s should differ from %s" % (uptime1, uptime2)
        self.assertNotEqual(uptime1, uptime2, msg)
