#
# SPDX-License-Identifier: MIT
#

import os

from oeqa.runtime.case import OERuntimeTestCase
from oeqa.runtime.decorator.package import OEHasPackage
from oeqa.core.decorator.oetimeout import OETimeout

class DtcTestSuite(OERuntimeTestCase):
    """
    dump devicetree
    """
    @OETimeout(120)
    @OEHasPackage(['dtc'])
    def test_dtc(self):
        cmd = "dtc -I fs -O dts /sys/firmware/devicetree/base"
        status, output = self.target.run(cmd, 60)
        self.assertEqual(status, 0, msg='\n'.join([cmd, output]))
